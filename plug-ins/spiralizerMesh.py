import sys
import math
import maya.cmds as cmds
import maya.OpenMaya as om
import maya.OpenMayaMPx as ompx

# node name
kPluginNodeTypeName = 'spiralizerMesh'
spiralizerMeshID = om.MTypeId(0x00000006)

# Attribute names
curveInputLong = 'inputCurve'
curveInputShort = 'ic'
clockwiseLong = 'clockwise'
clockwiseShort = 'cw'
percentLong = 'percent'
percentShort = 'p'
samplesULong = 'samplesU'
samplesUShort = 'su'
samplesVLong = 'samplesV'
samplesVShort = 'sv'
turnsLong = 'turns'
turnsShort = 't'
radiusSpiralLong = 'radiusSpiral'
radiusSpiralShort = 'rs'
radiusCylinderLong = 'radiusCylinder'
radiusCylinderShort = 'rcs'
startCapsLong = 'subdividionStartCap'
startCapsShort = 'ssc'
endCapsLong = 'subdivisionEndCap'
endCapsShort = 'sec'
twistLong = 'twistCylinder'
twisterShort = 'tc'
radiusSpiralRampLong = 'radiusSpiralRamp'
radiusSpiralRampShort = 'rsr'
radiusOptionsLong = 'radiusOptions'
radiusOptionsShort = 'rop'
distributionSpiralRampLong = 'distributionSpiralRamp'
distributionSpiralRampShort = 'dsr'
distributionOptionsLong = 'distributionOptions'
distributionOptionsShort = 'do'
radiusCylinderRampLong = 'radiusCylinderRamp'
radiusCylinderRampShort = 'rcr'
twistCylinderRampLong = 'twistCylinderRamp'
twistCylinderRampShort = 'tcr'
offsetLong = 'offset'
offsetShort = 'off'
meshOutputLong = 'meshOutput'
meshOutputShort = 'mo'


# crete class for spiralizer mesh node
class SpiralizerMeshNode(ompx.MPxNode):
    # Create MObjects to input and output
    curveInput = om.MObject()
    clockwiseInput = om.MObject()
    percentInput = om.MObject()
    samplesUInput = om.MObject()
    samplesVInput = om.MObject()
    turnsInput = om.MObject()
    radiusSpiralInput = om.MObject()
    radiusCylinderInput = om.MObject()
    samplesStartCap = om.MObject()
    samplesEndCap = om.MObject()
    twisterCylinderInput = om.MObject()
    radiusSpiralRampInput = om.MObject()
    radiusSpiralRampOptionsInput = om.MObject()
    distributionRampInput = om.MObject()
    distributionOptionsInput = om.MObject()
    radiusCylinderRampInput = om.MObject()
    twistCylinderRampInput = om.MObject()
    offsetInput = om.MObject()
    meshOutput = om.MObject()

    def __init__(self):
        ompx.MPxNode.__init__(self)
        # Both MFn classes are slow in initialization. So, it is preferable to reuse them throughout the process.
        self.mfnCurve = om.MFnNurbsCurve()
        self.mfnDepNode = om.MFnDependencyNode()

        self.pointPosition = om.MPoint()
        self.newPointFrontQuaternion = om.MQuaternion()
        self.tmpQuaternion = om.MQuaternion()
        self.vertexStartCap = om.MVector()


        # override
    def postConstructor(self, *args):
        # Initialize radiusRampInput
        # Get MPlug of radiusRampInput and use to initialize a MRampAttribute to set new values.
        plug = om.MPlug(self.thisMObject(), SpiralizerMeshNode.radiusSpiralRampInput)
        rampAttr = om.MRampAttribute(plug)
        try:
            # radiusRampInput will be a constant graph
            # |o-------|
            # |________|
            rampAttr.setValueAtIndex(1.0, 0)
        except:
            pass

        # Initialize distributionRampInput
        # Get MPlug of distributionRampInput and use to initialize a MRampAttribute to set a new values
        plug = om.MPlug(self.thisMObject(), SpiralizerMeshNode.distributionRampInput)
        rampAttr = om.MRampAttribute(plug)
        try:
            # distributionRampInput will be an ascending graph
            # |    /o|
            # |  /   |
            # |o/____|
            # Set first index to 0.0 value
            rampAttr.setValueAtIndex(0.0, 0)

            # Add new index (1) ...
            indexArray = om.MFloatArray()
            indexArray.append(1.0)
            # with value 1.0 ...
            valueArray = om.MFloatArray()
            valueArray.append(1.0)
            # and set interpolation to linear.
            interpArray = om.MIntArray()
            interpArray.append(1)

            rampAttr.addEntries(indexArray,
                                valueArray,
                                interpArray)
        except :
            pass

        # Initialize radiusCylinderRampInput
        # Get MPlug of radiusCylinderRampInput and use to initialize a MRampAttribute to set a new values.
        plug = om.MPlug(self.thisMObject(), SpiralizerMeshNode.radiusCylinderRampInput)
        rampAttr = om.MRampAttribute(plug)
        try:
            # radiusRampInput will be a constant graph
            # |o-------|
            # |________|
            rampAttr.setValueAtIndex(1.0, 0)
        except:
            pass

        # Initialize twistCylinderRampInput
        # Get MPlug of twistCylinderRampInput and use to initialize a MRampAttribute to set a new values.
        plug = om.MPlug(self.thisMObject(), SpiralizerMeshNode.twistCylinderRampInput)
        rampAttr = om.MRampAttribute(plug)
        try:
            # radiusRampInput will be a constant graph
            # |o-------|
            # |________|
            rampAttr.setValueAtIndex(1.0, 0)
        except:
            pass

    # override
    def compute(self, plug, dataBlock):
        # Verify if have something connected in meshOutput plug.
        # Otherwise, SpiralizerMeshNode will not process nothing
        if plug == SpiralizerMeshNode.meshOutput:
            ############################################################################################################
            # Before start process, need get some handles to access external informations.
            #
            # Create input curve handle
            curveHandle = dataBlock.inputValue(SpiralizerMeshNode.curveInput)
            curveObjInput = curveHandle.asNurbsCurveTransformed()

            # Create clockwise boolean handle
            clockwiseHandle = dataBlock.inputValue(SpiralizerMeshNode.clockwiseInput)
            clockwiseStatus = clockwiseHandle.asBool()

            # create percent value handle
            percentHandle = dataBlock.inputValue(SpiralizerMeshNode.percentInput)
            percentValue = percentHandle.asFloat()
            percentValue *= 0.01 # normalize to 0.0 - 1.0

            # Create samplesU handle
            samplesUHandle = dataBlock.inputValue(SpiralizerMeshNode.samplesUInput)
            samplesUValue = samplesUHandle.asInt()

            # Create samplesV handle
            samplesVHandle = dataBlock.inputValue(SpiralizerMeshNode.samplesVInput)
            samplesVValue = samplesVHandle.asInt()

            # Create turn handle
            turnHandle = dataBlock.inputValue(SpiralizerMeshNode.turnsInput)
            turnValue = turnHandle.asFloat()

            # Create radiusSpiral handle
            radiusSpiralHandle = dataBlock.inputValue(SpiralizerMeshNode.radiusSpiralInput)
            radiusSpiralValue = radiusSpiralHandle.asFloat()

            # Create radiusCylinder handle
            radiusCylinderHandle = dataBlock.inputValue(SpiralizerMeshNode.radiusCylinderInput)
            radiusCylinderValue = radiusCylinderHandle.asFloat()

            # Create startCap handle
            startCapHandle = dataBlock.inputValue(SpiralizerMeshNode.samplesStartCap)
            startCapValue = startCapHandle.asInt()

            # Create endCap handle
            endCapHandle = dataBlock.inputValue(SpiralizerMeshNode.samplesEndCap)
            endCapValue = endCapHandle.asInt()

            # Create twistCylinder handle
            twistCylinderHandle = dataBlock.inputValue(SpiralizerMeshNode.twisterCylinderInput)
            twistCylinderValue = twistCylinderHandle.asFloat()

            # Create rampRadius MRampAttribute (in case of MRamps we need use MRampAttribute to access this plug)
            rampRadiusAttr = om.MRampAttribute(self.thisMObject(), SpiralizerMeshNode.radiusSpiralRampInput)

            # Create radiusRampOptions handle
            rampRadiusOptionsHandle = dataBlock.inputValue(SpiralizerMeshNode.radiusSpiralRampOptionsInput)
            rampRadiusOptionsValue = rampRadiusOptionsHandle.asShort()

            # Create rampDistribution MRampAttribute
            rampDistribAttr = om.MRampAttribute(self.thisMObject(), SpiralizerMeshNode.distributionRampInput)

            # Create distributionRampOptions handle
            rampDistributionOptionsHandle = dataBlock.inputValue(SpiralizerMeshNode.distributionOptionsInput)
            rampDistributionOptionsValue = rampDistributionOptionsHandle.asShort()

            # Create radiusCylinderRampAttr MRampAttribute
            radiusCylinderRampAttr = om.MRampAttribute(self.thisMObject(), SpiralizerMeshNode.radiusCylinderRampInput)

            # Create twistCylinderRampAttr MRampAttribute
            twistCylinderRampAttr = om.MRampAttribute(self.thisMObject(), SpiralizerMeshNode.twistCylinderRampInput)

            # Create offset handle
            offsetHandle = dataBlock.inputValue(SpiralizerMeshNode.offsetInput)
            offsetValue = offsetHandle.asFloat()

            # Create output mesh handle
            meshOutputHandle = dataBlock.outputValue(SpiralizerMeshNode.meshOutput)

            ############################################################################################################
            # Process to define up-vector and front-vector of curve
            #

            self.mfnCurve.setObject(curveObjInput)
            # Initialize vectors of transform curve
            curveTransformXVector = om.MVector().xAxis
            curveTransformYVector = om.MVector().yAxis
            curveTransformZVector = om.MVector().zAxis

            initialUpVector = om.MVector()
            initialFrontVector = om.MVector()
            initialSideVector = om.MVector()

            # Get inclusiveMatrix from input curve
            thisNode = SpiralizerMeshNode.thisMObject(self)
            self.mfnDepNode.setObject(thisNode)
            # Get MPlug of curveInput. Its necessary to access curve node..
            # to get your dagPath to this curve.
            inputCurveMPlug = self.mfnDepNode.findPlug(SpiralizerMeshNode.curveInput)
            curveShapeMPlug = inputCurveMPlug.source()
            curveShapeMObject = curveShapeMPlug.node()
            curveShapeDagPath = om.MDagPath()
            om.MDagPath.getAPathTo(curveShapeMObject, curveShapeDagPath)
            # With a dagPath to curve, we can get inclusiveMatrix
            inclusiveMatrixCurve = curveShapeDagPath.inclusiveMatrix()

            # Use inclusiveMatrixCurve to calculate a new values to initial vectors
            curveTransformXVector = curveTransformXVector * inclusiveMatrixCurve
            curveTransformYVector = curveTransformYVector * inclusiveMatrixCurve
            curveTransformZVector = curveTransformZVector * inclusiveMatrixCurve

            # Use MItCurveCV to get firsts CV points to calculate the initial vector of curve
            mItCurve = om.MItCurveCV(curveObjInput)
            firstCV = om.MPoint()
            secondCV = om.MPoint()

            while not mItCurve.isDone():
                # Get first CV position
                if mItCurve.index() == 0:
                    firstCV = mItCurve.position()
                else:
                    secondCV = mItCurve.position()
                    # If firstCV and secondCV have the same position, need get other CV
                    if not firstCV == secondCV:
                        break
                mItCurve.next()

            initialFrontVector = om.MVector(secondCV - firstCV)
            initialFrontVector.normalize()

            # Calculate 3 initial vectors based in initialFrontVector
            # 3 firsts cases work if the initialFrontVector is the same as one of the orthogonal vectors
            if initialFrontVector.isParallel(curveTransformYVector):
                initialSideVector = curveTransformZVector
                initialUpVector = (curveTransformXVector * -1 if initialFrontVector.isEquivalent(curveTransformYVector)
                                   else curveTransformYVector)
            elif initialFrontVector.isParallel(curveTransformXVector):
                initialSideVector = (curveTransformZVector if initialFrontVector.isEquivalent(curveTransformXVector)
                                     else curveTransformZVector * -1)
            elif initialFrontVector.isParallel(curveTransformZVector):
                initialSideVector = (curveTransformXVector * -1 if initialFrontVector.isEquivalent(curveTransformZVector)
                                     else curveTransformXVector)
                initialUpVector = curveTransformYVector
            else: # common case
                _tmpYAxis = curveTransformYVector
                initialSideVector = initialFrontVector ^ _tmpYAxis
                initialSideVector.normalize()
                initialUpVector = initialSideVector ^ initialFrontVector
                initialUpVector.normalize()

            ############################################################################################################
            # Start process to calculate each CV (samples) of spiral along path
            #

            # Calculate the percentage of the curve that will be used.
            # Get total length of path
            totalLengthCurveInput = self.mfnCurve.length()
            # Calculate percent length
            lengthCurve = totalLengthCurveInput * percentValue

            # Calculate the total amount of samples along the path
            _calculatedSamples = int(samplesUValue) * int(turnValue)
            _calculatedSamples = (1 if _calculatedSamples == 0 else _calculatedSamples)
            lengthPart = lengthCurve / _calculatedSamples

            # Calculate the turn value for each sample
            completeTurn = (2 * math.pi) * turnValue
            turnPart = (completeTurn / _calculatedSamples)

            # Create a NurbsCurveData to store all data of new curve
            spiralCurveData = om.MFnNurbsCurveData()
            spiralCurveObj = spiralCurveData.create()

            # Create empty MPointArray to store vertex/CV coordinates
            controlVertices = om.MPointArray()

            # Variables to represent local vectors for each sample of input curve
            localFrontVector = om.MVector()
            localSideVector = om.MVector()
            localUpVector = om.MVector()

            # This condition guarantee processing only if percent great than 0
            if percentValue > 0:
                for i in range(_calculatedSamples):
                    # Calculate the current raw length of the path based on percent value.
                    currentLength = i * lengthPart
                    # User can choose between two mapping types of ramps
                    if rampDistributionOptionsValue == 0:
                        # Calculate percent of totalLengthCurveInput (path)
                        percentLength = currentLength / totalLengthCurveInput
                    else:
                        # Calculate percent of lengthCurve
                        percentLength = currentLength / lengthCurve
                    # OMG.. MScriptUtil to get reference values. Get length value based on ramps.
                    util = om.MScriptUtil()
                    util_ptr = util.asFloatPtr()
                    rampDistribAttr.getValueAtPosition(percentLength, util_ptr)
                    percentResult = om.MScriptUtil.getFloat(util_ptr)

                    # currentLength result in new value based on ramps
                    currentLength = lengthCurve * percentResult
                    # Get curve param to current length of path
                    curveParameter = self.mfnCurve.findParamFromLength(currentLength)

                    # Get coords from current sample (current length)
                    pointPosition = om.MPoint()
                    self.mfnCurve.getPointAtParam(curveParameter, pointPosition)

                    # Get tangent from current samples (current length)
                    tangent = om.MVector()
                    tangent = self.mfnCurve.tangent(curveParameter)

                    # Calculate local vectors of current pointPosition
                    # At first sample, use initial vectors as a base
                    if i == 0:
                        localFrontVector = tangent
                        localSideVector = localFrontVector ^ initialUpVector
                        localUpVector = localSideVector ^ localFrontVector

                    # Others samples
                    else:
                        localFrontVector = tangent
                        localSideVector = localFrontVector ^ localUpVector
                        localUpVector = localSideVector ^ localFrontVector

                    localFrontVector.normalize()
                    localSideVector.normalize()
                    localUpVector.normalize()

                    # Create initial coordinates to new spiralControlVertice
                    _currentCurveValue = (turnPart * i)
                    # Get current relative radius value using MRamp
                    if rampRadiusOptionsValue == 0:
                        percentCurve = (i * lengthPart)/totalLengthCurveInput
                    elif rampRadiusOptionsValue == 1:
                        percentCurve = (i * lengthPart) / lengthCurve
                    else:
                        percentCurve = (i * lengthPart) / lengthCurve
                    # Again.. more MScriptUtils...
                    util = om.MScriptUtil()
                    util_ptr = util.asFloatPtr()
                    rampRadiusAttr.getValueAtPosition(percentCurve, util_ptr)
                    currentRadiusValue = om.MScriptUtil.getFloat(util_ptr)

                    # Control clockwise or counterclockwise
                    _resultRadius = currentRadiusValue * radiusSpiralValue
                    if clockwiseStatus:

                        x = 0
                        y = math.cos(_currentCurveValue - offsetValue) * _resultRadius
                        z = math.sin(_currentCurveValue - offsetValue) * _resultRadius
                        spiralControlVertice = om.MVector(x, y, z)
                    else:

                        x = 0
                        y = math.sin(_currentCurveValue - offsetValue) * _resultRadius
                        z = math.cos(_currentCurveValue - offsetValue) * _resultRadius
                        spiralControlVertice = om.MVector(x, y, z)

                    # Calculate correct 'aim/front vector' orientation to new point
                    newPointFrontVector = om.MVector().xAxis
                    newPointFrontQuaternion = om.MQuaternion(newPointFrontVector, localFrontVector)

                    # Calculate 'up' vector
                    tmpUpVector = om.MVector().yAxis
                    tmpUpVector = tmpUpVector.rotateBy(newPointFrontQuaternion)
                    angle = tmpUpVector.angle(localUpVector)
                    tmpQuaternion = om.MQuaternion(angle, localFrontVector)
                    if not localUpVector.isEquivalent(tmpUpVector.rotateBy(tmpQuaternion), 1.0e-4):
                        angle = (2 * math.pi) - angle
                        tmpQuaternion = om.MQuaternion(angle, localFrontVector)

                    # Here we union two transform (aim/front vector and up vector to one single quaternion) to apply
                    # the transformation on initial coordinates of spiral (spiralControlVertice)
                    newPointFrontQuaternion *= tmpQuaternion
                    spiralControlVertice = spiralControlVertice.rotateBy(newPointFrontQuaternion)

                    # Finally, adjust spiralControlVertice to new position using MTransformMatrix.
                    tMat = om.MTransformationMatrix()
                    tMat.setTranslation(om.MVector(pointPosition), om.MSpace.kWorld)
                    tmpMatrix = tMat.asMatrix()
                    # We need convert spiralControlVertice(MVector) to MPoint for use with MTransformMatrix
                    spiralControlVertice = om.MPoint(spiralControlVertice)
                    spiralControlVertice = spiralControlVertice * tmpMatrix

                    # Now, append this new coordinate to the array. The spiral will be created after loop.
                    controlVertices.append(spiralControlVertice)

                # Using controlVertices(MPointArray) to create a spiral em memory
                self.mfnCurve.createWithEditPoints(controlVertices,
                                                   3,
                                                   om.MFnNurbsCurve.kOpen,
                                                   False,
                                                   False,
                                                   False,
                                                   spiralCurveObj)

                ############################################################################################################
                # Starting the process of creating cylinder mesh.
                # First, calculate spiralLocalVectors in a similar way to the path
                self.mfnCurve.setObject(spiralCurveObj)

                # Initialize vectors of spiralCurve
                spiralTransformXVector = om.MVector().xAxis
                spiralTransformYVector = om.MVector().yAxis
                spiralTransformZVector = om.MVector().zAxis

                initialSpiralUpVector = om.MVector()
                initialSpiralFrontVector = om.MVector()
                initialSpiralSideVector = om.MVector()

                # Use inclusiveMatrixCurve of path to calculate a new values to initial vectors
                spiralTransformXVector = spiralTransformXVector * inclusiveMatrixCurve
                spiralTransformYVector = spiralTransformYVector * inclusiveMatrixCurve
                spiralTransformZVector = spiralTransformZVector * inclusiveMatrixCurve

                # Use MItCurveCV to get firsts CV points to calculate the initial vector of spiral
                mItCurve = om.MItCurveCV(spiralCurveObj)
                firstCV = om.MPoint()
                secondCV = om.MPoint()

                while not mItCurve.isDone():
                    # Get first CV position
                    if mItCurve.index() == 0:
                        firstCV = mItCurve.position()
                    else:
                        secondCV = mItCurve.position()
                        # If firstCV and secondCV have the same position, need get other CV
                        if not firstCV == secondCV:
                            break
                    mItCurve.next()

                initialSpiralFrontVector = om.MVector(secondCV - firstCV)
                initialSpiralFrontVector.normalize()

                # Calculate 3 initial vectors based in initialSpiralFrontVector
                # 3 firsts cases work if the initialSpiralFrontVector is the same as one of the orthogonal vectors
                if initialSpiralFrontVector.isParallel(spiralTransformYVector):
                    initialSpiralSideVector = spiralTransformZVector
                    initialSpiralUpVector = (curveTransformXVector * -1
                                             if initialSpiralFrontVector.isEquivalent(spiralTransformYVector)
                                             else spiralTransformYVector)
                elif initialSpiralFrontVector.isParallel(spiralTransformXVector):
                    initialSpiralSideVector = (spiralTransformZVector
                                               if initialSpiralFrontVector.isEquivalent(spiralTransformXVector)
                                               else spiralTransformZVector * -1)
                    initialSpiralUpVector = spiralTransformYVector
                elif initialSpiralFrontVector.isParallel(spiralTransformZVector):
                    initialSpiralSideVector = (spiralTransformXVector * -1
                                               if initialSpiralFrontVector.isEquivalent(spiralTransformZVector)
                                               else spiralTransformXVector)
                    initialSpiralUpVector = spiralTransformYVector
                else:
                    _tmpYAxis = spiralTransformYVector
                    initialSpiralSideVector = initialSpiralFrontVector ^_tmpYAxis
                    initialSpiralSideVector.normalize()
                    initialSpiralUpVector = initialSpiralSideVector ^ initialSpiralFrontVector
                    initialSpiralUpVector.normalize()

                ############################################################################################################
                # Start process to calculate point (samples) along spiral curve
                #

                # Calcualte total length of spiral curve
                spiralLengthCurve = self.mfnCurve.length()
                spiralLengthPart = spiralLengthCurve / _calculatedSamples

                # Variables to represent local vectors for each samples of spiral curve
                spiralLocalFrontVector = om.MVector()
                spiralLocalSideVector = om.MVector()
                spiralLocalUpVector = om.MVector()

                # Create a meshData to store all data of new mesh
                spiralMeshData = om.MFnMeshData()
                spiralMeshObj = spiralMeshData.create()

                # Create a empty MPointArray to store vertex coordinates
                vertexArray = om.MFloatPointArray()

                subAxis = samplesVValue
                subHeight = _calculatedSamples
                startCaps = startCapValue
                endCaps = endCapValue
                radius = radiusCylinderValue

                # Create Initial parameters of cylinder mesh
                numPolygons = (subAxis * subHeight) + (subAxis * startCaps) + (subAxis * endCaps) + (subAxis * 2)
                numVertices = subAxis * (subHeight + 1) + (subAxis * startCaps) + (subAxis * endCaps) + 2
                numUVs = (subAxis + 1) * (subHeight + startCaps + endCaps + 1) + subAxis + subAxis
                # "subAxis + subAxis" n faces of startCaps + n faces of endCaps

                # Calculate radian angle for each face to complete a cylinder
                stepsRadian = (2 * math.pi) / subAxis


                # Create a empty MFloatPointArray to store vertex coordinates
                vertexArray = om.MFloatPointArray()

                # Create a MVector to store last spiral coordinate
                spiralLastPos = om.MVector(0,0,0)

                for i in range(_calculatedSamples + 1):
                    # Calculate the current raw length
                    spiralCurrentLength = i * spiralLengthPart

                    spiralCurveParameter = self.mfnCurve.findParamFromLength(spiralCurrentLength)

                    self.pointPosition = om.MPoint()
                    self.mfnCurve.getPointAtParam(spiralCurveParameter, self.pointPosition)

                    # Get tangent from current sample
                    tangent = om.MVector()
                    tangent = self.mfnCurve.tangent(spiralCurveParameter)

                    # Calculate local vector of curent pointPosition
                    # At first sample, use initial vectors as a base
                    if i == 0:
                        spiralLocalFrontVector = tangent
                        spiralLocalSideVector = spiralLocalFrontVector ^ initialSpiralUpVector
                        spiralLocalUpVector = spiralLocalSideVector ^ spiralLocalFrontVector
                    # Other samples
                    else:
                        spiralLocalFrontVector = tangent
                        spiralLocalSideVector = spiralLocalFrontVector ^ spiralLocalUpVector
                        spiralLocalUpVector = spiralLocalSideVector ^ spiralLocalFrontVector

                    spiralLocalFrontVector.normalize()
                    spiralLocalSideVector.normalize()
                    spiralLocalUpVector.normalize()

                    # Calculate correct 'aim/front vector' orientation to new vertex point of spiral
                    newPointFrontVector = om.MVector().xAxis
                    self.newPointFrontQuaternion = om.MQuaternion(newPointFrontVector, spiralLocalFrontVector)

                    # calculate 'up' vector of spiral
                    tmpUpVector = om.MVector().yAxis
                    tmpUpVector = tmpUpVector.rotateBy(self.newPointFrontQuaternion)
                    angle = tmpUpVector.angle(spiralLocalUpVector)
                    self.tmpQuaternion = om.MQuaternion(angle, spiralLocalFrontVector)
                    if not spiralLocalUpVector.isEquivalent(tmpUpVector.rotateBy(self.tmpQuaternion), 1.0e-4):
                        angle = (2 * math.pi) - angle
                        self.tmpQuaternion = om.MQuaternion(angle, spiralLocalFrontVector)

                    ####################################################################################################
                    ####################################################################################################
                    # vertexArray (MFloatPointArray)
                    #
                    # Calculate all vertexes coordinates of cylinder.
                    ####################################################################################################
                    percentCurve = spiralCurrentLength / spiralLengthCurve
                    # Again again.. more MScriptUtils...
                    # Get radiusCylinderRampAttr (MRampAttribute) value
                    util = om.MScriptUtil()
                    util_ptr = util.asFloatPtr()
                    radiusCylinderRampAttr.getValueAtPosition(percentCurve, util_ptr)
                    currentRadiusCylinderValue = om.MScriptUtil.getFloat(util_ptr)

                    # Get twistCylinderRampAttr (MRampAttribute) value
                    util = om.MScriptUtil()
                    util_ptr = util.asFloatPtr()
                    twistCylinderRampAttr.getValueAtPosition(percentCurve, util_ptr)
                    currentTwistCylinderValue = om.MScriptUtil.getFloat(util_ptr)



                    if i == 0:
                        # Store first point from Spiral path
                        spiralLastPos = om.MVector(0, 0, 0)
                        spiralLastPos = self.__transformPosition(spiralLastPos)

                        # Create vertex for startCap
                        startRadius = float(radius) * currentRadiusCylinderValue
                        stepsCaps = startRadius / (startCaps + 1)
                        for sc in range(startCaps):
                            for a in range(subAxis):
                                _tmpValue = (stepsCaps * (sc + 1))
                                x = 0
                                y = math.cos(stepsRadian * a + (twistCylinderValue * currentTwistCylinderValue)) * _tmpValue
                                z = math.sin(stepsRadian * a + (twistCylinderValue * currentTwistCylinderValue)) * _tmpValue
                                spiralMeshVertex = om.MVector(x, y, z)
                                # Apply transformations...
                                spiralMeshVertex = self.__transformPosition(spiralMeshVertex)

                                # Now, append this new coordinates to the array. The spiral mesh will be created after
                                # all calculated vertex.
                                vertexArray.append(om.MFloatPoint(spiralMeshVertex))

                                self.__createLocator(spiralMeshVertex)
                        # Calculated center vertex for startCap. Need be append at the end of all other
                        # vertex/iterations.
                        vertexCap = om.MVector(0,0,0)
                        self.vertexStartCap = self.__transformPosition(vertexCap)



                    # Calculates vertex position to side faces
                    for a in range(subAxis):
                        x = 0
                        y = math.cos(stepsRadian * a + (twistCylinderValue * currentTwistCylinderValue)) * (float(radius) * currentRadiusCylinderValue)
                        z = math.sin(stepsRadian * a + (twistCylinderValue * currentTwistCylinderValue)) * (float(radius) * currentRadiusCylinderValue)

                        spiralMeshVertex = om.MVector(x, y, z)
                        # Apply transformations...
                        spiralMeshVertex = self.__transformPosition(spiralMeshVertex)
                        # ... and append to the array.
                        vertexArray.append(om.MFloatPoint(spiralMeshVertex))

                        self.__createLocator(spiralMeshVertex)

                    # Run this only in last iteration to make vertex of endCaps
                    if i == (_calculatedSamples):
                        endRadius = float (radius) * currentRadiusCylinderValue
                        stepsCaps = endRadius / (endCaps + 1)
                        # Attention! We need to create these vertexes using a reverse order of range
                        reverseRange = range(endCaps)
                        reverseRange.reverse()
                        for ec in reverseRange:
                            for a in range(subAxis):
                                _tmpValue = (stepsCaps * (ec + 1))
                                x = 0
                                y = math.cos(stepsRadian * + (twistCylinderValue * currentTwistCylinderValue)) * _tmpValue
                                z = math.sin(stepsRadian * + (twistCylinderValue * currentTwistCylinderValue)) * _tmpValue

                                spiralMeshVertex = om.MVector(x, y, z)
                                # Apply transformations...
                                spiralMeshVertex = self.__transformPosition(spiralMeshVertex)
                                # ... and append to the array.
                                vertexArray.append(om.MFloatPoint(spiralMeshVertex))

                                self.__createLocator(spiralMeshVertex)

                        # Append two vertex for the center of each caps.
                        # Calculated vertex for endCap
                        vertexEndCap = om.MVector(0, 0, 0)
                        # Apply transformations
                        vertexEndCap = self.__transformPosition(vertexEndCap)

                        vertexArray.append(om.MFloatPoint(self.vertexStartCap))
                        vertexArray.append(om.MFloatPoint(vertexEndCap))
                        self.__createLocator(self.vertexStartCap)
                        self.__createLocator(vertexEndCap)


                ########################################################################################################
                ########################################################################################################
                # polygonCounts (MIntArray)
                #
                # Store amount of edges for each face/poly in order of creation.
                # ex: [4,4,3] - two quad faces and one triangular face.
                ########################################################################################################
                polygonCounts = om.MIntArray()

                # This algorithm first creates all side faces and in the last, faces of caps.
                # But the cap only considered faces connected directly with vertexes of  the cap. The other faces are
                # considered side faces
                totalExpandedSideFaces = (subAxis * subHeight) + (subAxis * startCaps) + (subAxis * endCaps)
                for c in range(totalExpandedSideFaces):
                    # "4" because side faces are quad faces.
                    polygonCounts.append(4)

                # Store cap faces (only faces connected directly with cap vertexes)
                totalIsolatedCapFaces = subAxis * 2
                for c in range(totalIsolatedCapFaces):
                    # "3" because cap faces are triangular faces.
                    polygonCounts.append(3)

                ########################################################################################################
                ########################################################################################################
                # polyConnections (MIntArray)
                #
                # Store a vertex IDs  in sequence to create each face declared in polygonCounts
                # ex: polygonCounts = [4,3] - a quad face and a triangular face.
                #     polyConnections = [0,1,2,3,1,4,2]
                #                       - in order we get first 4 vertex IDs to create a quad face (0,1,2,3).
                #                       - to create a next face, a triangular face, use the next sequence (1,4,2)
                #
                #    3 ------- 2 \
                #    |         |  \
                #    |         |   4
                #    |         |  /
                #    0 ------- 1 /
                #
                ########################################################################################################

                # polyConnections will store a sequence of vertex IDs to create all faces of the cylinder.
                polyConnections = om.MIntArray()

                # First we need one list with all vertexes IDs
                vertexesIDs = [i for i in range(numVertices - 2)]

                # Create two list to store vertex IDs to side faces and cap faces
                sideVertexesIDs = []
                capsVertexesIDs = []

                # We need to consider for each row side faces are surrounded by two edge loops. So, total edge loops are
                # total of side faces + 1. Finally, if we get total edge loops and multiply with subAxis we have a total
                # of vertexes needed.
                totalEdgeLoops = subHeight + startCaps + endCaps + 1
                for i in range(totalEdgeLoops):
                    start = i * subAxis
                    end = start + subAxis
                    sideVertexesIDs.append(vertexesIDs[start: end])

                # Append two last vertex for caps
                capsVertexesIDs.append(int(numVertices - 2))
                capsVertexesIDs.append(int(numVertices - 2) + 1)

                # With all vertexes of each edge loops defined, we need to start define correct vertex, in correct
                # order, to make a new faces.
                # To do this, it is necessary to iterate through the total number of side-face divisions.
                for i in range(subHeight + startCaps + endCaps):
                    # Get the equivalent of the first and second loop for each iteration
                    firstLoop = sideVertexesIDs[i]
                    secondLoop = sideVertexesIDs[i + 1]
                    # This iteration run for all faces in a row.
                    for f in range(subAxis):
                        index0 = f % subAxis
                        index1 = (f + 1) % subAxis
                        index2 = (f + 1) % subAxis
                        index3 = f % subAxis
                        vtxId0 = firstLoop[index0]
                        vtxId1 = firstLoop[index1]
                        vtxId2 = secondLoop[index2]
                        vtxId3 = secondLoop[index3]
                        polyConnections.append(vtxId0)
                        polyConnections.append(vtxId1)
                        polyConnections.append(vtxId2)
                        polyConnections.append(vtxId3)

                # Make equivalent process to define correct vertices to startCap and endCap

                # startCap
                # We need get first loop of side vertexes.
                vertexStartCap = sideVertexesIDs[0]
                for i in range(subAxis):
                    index0 = i
                    index1 = 0
                    index2 = (i + 1) % subAxis
                    vtxId0 = vertexStartCap[index0]
                    vtxId1 = capsVertexesIDs[index1]
                    vtxId2 = vertexStartCap[index2]
                    polyConnections.append(vtxId0)
                    polyConnections.append(vtxId1)
                    polyConnections.append(vtxId2)

                # endCap
                # We need get last loop of side vertexes.
                vertexEndCap = sideVertexesIDs[-1]
                for i in range(subAxis):
                    index0 = i
                    index1 = (i + 1) % subAxis
                    index2 = 1
                    vtxId0 = vertexEndCap[index0]
                    vtxId1 = vertexEndCap[index1]
                    vtxId2 = capsVertexesIDs[index2]
                    polyConnections.append(vtxId0)
                    polyConnections.append(vtxId1)
                    polyConnections.append(vtxId2)

                ########################################################################################################
                ########################################################################################################
                # U (MFloatArray) and V (MFloatArray)
                #
                # Store uv coordinates
                ########################################################################################################
                U = om.MFloatArray(numUVs)
                V = om.MFloatArray(numUVs)

                # Define some parameters
                # Calculate amount of UVs points to expanded side faces
                totalExpandedSideUVs = (subAxis + 1) * (subHeight + startCaps + endCaps + 1)
                # Calculate total of 'edge loops' made of UVs points to all side faces.
                totalEdgeLoopsUVs = totalExpandedSideUVs / (subAxis + 1)

                # Start calculate U coordinates from side faces
                # Calculate a length of UVs 'edge loop'
                moduleUV = totalExpandedSideUVs / (subHeight + startCaps + endCaps + 1)
                distributionU = 1.0 / subAxis
                for u in range(totalExpandedSideUVs):
                    index = u % moduleUV
                    U[u] = index * distributionU

                # Calculate V coordinates from side faces
                distributionV = 1.0 / (subHeight + startCaps + endCaps + 2)
                for v in range(totalExpandedSideUVs):
                    V[v] = ((int(v) / int(moduleUV)) * distributionV) + distributionV

                # Calculate U coordinates from startcaps.
                halfDistributionU = distributionU / 2.0
                for u in range(numUVs - totalExpandedSideUVs):
                    index = u % subAxis
                    U[u + totalExpandedSideUVs] = (index * distributionU) + halfDistributionU

                # Calculate V coordinates from endCaps
                for v in range(numUVs - totalExpandedSideUVs):
                    index = (int(v) / int(subAxis))
                    V[v + totalExpandedSideUVs] = (1.0 if index else 0.0)

                ########################################################################################################
                ########################################################################################################
                # create a new Mesh
                #
                # Create a new mesh using all datas
                ########################################################################################################
                mfnMesh = om.MFnMesh()

                mfnMesh.create(numVertices,
                               numPolygons,
                               vertexArray,
                               polygonCounts,
                               polyConnections,
                               U, V,
                               spiralMeshObj)

                ########################################################################################################
                ########################################################################################################
                # uvIDs (MIntArray)
                #
                # uvIDs store a sequence of UVs IDs to mount each UV patches. Similar to polyConnections
                ########################################################################################################
                uvIDs = om.MIntArray()

                # Two list to store UVs IDs of side faces and cap faces.
                uvsIDsSide = []
                uvsIDsCaps = []

                # Make interation to fill each 'edge loop' of UVs IDs (side faces)
                i = 0
                for a in range(totalEdgeLoopsUVs):
                    _tmpArray = []
                    for f in range(subAxis + 1):
                        _tmpArray.append(i)
                        i += 1
                    uvsIDsSide.append(_tmpArray)

                # Fill with UVs IDs to both caps
                for c in range(2):
                    _tmpArray = []
                    for s in range(subAxis):
                        _tmpArray.append(i)
                        i += 1
                    uvsIDsCaps.append(_tmpArray)

                # Start combination of UVs IDs (all side faces)
                for i in range(subHeight + startCaps + endCaps):
                    firstLoop = uvsIDsSide[i]
                    secondLoop = uvsIDsSide[i+1]
                    for f in range(subAxis):
                        index0 = f
                        index1 = (f + 1)
                        index2 = (f + 1)
                        index3 = f % subAxis
                        uvID0 = firstLoop[index0]
                        uvID1 = firstLoop[index1]
                        uvID2 = secondLoop[index2]
                        uvID3 = secondLoop[index3]
                        uvIDs.append(uvID0)
                        uvIDs.append(uvID1)
                        uvIDs.append(uvID2)
                        uvIDs.append(uvID3)

                # Combination of UVs IDs of startCap
                # Get first loop of side and cap UVs.
                baseCapUVs = uvsIDsSide[0]
                topCapUVs = uvsIDsCaps[0]
                for c in range(subAxis):
                    index0 = c
                    index1 = c
                    index2 = c + 1
                    uvID0 = baseCapUVs[index0]
                    uvID1 = topCapUVs[index1]
                    uvID2 = baseCapUVs[index2]
                    uvIDs.append(uvID0)
                    uvIDs.append(uvID1)
                    uvIDs.append(uvID2)

                # Combination of UVs IDs of endCap
                # Get last loop of side and cap UVs.
                baseCapUVs = uvsIDsSide[-1]
                topCapUVs = uvsIDsCaps[-1]
                for c in range(subAxis):
                    index0 = c
                    index1 = c + 1
                    index2 = c
                    uvID0 = baseCapUVs[index0]
                    uvID1 = baseCapUVs[index1]
                    uvID2 = topCapUVs[index2]
                    uvIDs.append(uvID0)
                    uvIDs.append(uvID1)
                    uvIDs.append(uvID2)

                ########################################################################################################
                ########################################################################################################
                # uvCounts (MIntArray)
                #
                # uvCounts store amount of edges for each patch of UVs in order of creation. Similar to polygonCounts
                # ex: [4,4,3] - two quad patches and one triangular patch.
                ########################################################################################################
                uvCounts = om.MIntArray()

                # Append quad patches for each side faces
                for i in range(totalExpandedSideFaces):
                    uvCounts.append(4)

                # Append triangular patches to cap faces
                for i in range(subAxis * 2):
                    uvCounts.append(3)

                ########################################################################################################
                ########################################################################################################
                # Assing UVs to new mesh
                #
                ########################################################################################################

                mfnMesh.assignUVs(uvCounts, uvIDs)

                meshOutputHandle.setMObject(spiralMeshObj)


            dataBlock.setClean(plug)

        else:
            return om.kUnknownParameter


    def __transformPosition(self, vectorPoint):
        # Here we make union two transform (aim/front vector and up vector to one single
        # quaternionr) to apply the transformation on initial coordinates of spiral mesh
        tmpPointFrontQuaternion = self.newPointFrontQuaternion * self.tmpQuaternion
        vectorPoint = vectorPoint.rotateBy(tmpPointFrontQuaternion)

        # Finally, adjust vectorPoint to new position using MTransformMatrix
        tMat = om.MTransformationMatrix()
        tMat.setTranslation(om.MVector(self.pointPosition), om.MSpace.kWorld)
        tmpMatrix = tMat.asMatrix()
        # We need convert vectorPoint (MVector) to MPoint for use with MTransformMatrix
        vectorPoint = om.MPoint(vectorPoint)
        vectorPoint = vectorPoint * tmpMatrix

        return vectorPoint


    def __createLocator(self, spiralMeshVertex):
        # loc = cmds.spaceLocator()
        # cmds.move(spiralMeshVertex.x, spiralMeshVertex.y, spiralMeshVertex.z, loc)
        # cmds.scale(0.1, 0.1, 0.1, loc)
        pass



def spiralizerMeshNodeCreator():
    return ompx.asMPxPtr(SpiralizerMeshNode())


def spiralizerMeshInitializer():
    # Initialize all attributes
    nAttr = om.MFnNumericAttribute()
    typedAttr = om.MFnTypedAttribute()
    eAttr = om.MFnEnumAttribute()

    ####################################################################################################################
    # curveInput
    SpiralizerMeshNode.curveInput = typedAttr.create(curveInputLong,
                                                     curveInputShort,
                                                     om.MFnNurbsCurveData.kNurbsCurve)
    typedAttr.setReadable(False)
    typedAttr.setWritable(True)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.curveInput)

    ####################################################################################################################
    # clockwiseInput
    SpiralizerMeshNode.clockwiseInput = nAttr.create(clockwiseLong,
                                                     clockwiseShort,
                                                     om.MFnNumericData.kBoolean, True)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.clockwiseInput)

    ####################################################################################################################
    # percentInput
    SpiralizerMeshNode.percentInput = nAttr.create(percentLong,
                                                   percentShort,
                                                   om.MFnNumericData.kFloat)
    nAttr.setMin(0.0)
    nAttr.setMax(100.0)
    nAttr.setDefault(100.0)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.percentInput)

    ####################################################################################################################
    # sampleUInput
    SpiralizerMeshNode.samplesUInput = nAttr.create(samplesULong,
                                                    samplesUShort,
                                                    om.MFnNumericData.kInt, 8)
    nAttr.setMin(1)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.samplesUInput)

    ####################################################################################################################
    # sampleVInput
    SpiralizerMeshNode.samplesVInput = nAttr.create(samplesVLong,
                                                    samplesVShort,
                                                    om.MFnNumericData.kInt, 6)
    nAttr.setMin(3)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.samplesVInput)

    ####################################################################################################################
    # turnsInput
    SpiralizerMeshNode.turnsInput = nAttr.create(turnsLong,
                                                 turnsShort,
                                                 om.MFnNumericData.kFloat, 20.0)
    nAttr.setMin(1)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.turnsInput)

    ####################################################################################################################
    # radiusSpiralInput
    SpiralizerMeshNode.radiusSpiralInput = nAttr.create(radiusSpiralLong,
                                                        radiusSpiralShort,
                                                        om.MFnNumericData.kFloat, 2.0)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.radiusSpiralInput)

    ####################################################################################################################
    # radiusCylinderInput
    SpiralizerMeshNode.radiusCylinderInput = nAttr.create(radiusCylinderLong,
                                                          radiusCylinderShort,
                                                          om.MFnNumericData.kFloat, 1.0)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.radiusCylinderInput)

    ####################################################################################################################
    # sampleStartCapInput
    SpiralizerMeshNode.samplesStartCap = nAttr.create(startCapsLong,
                                                      startCapsShort,
                                                      om.MFnNumericData.kInt, 2)
    nAttr.setMin(1)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.samplesStartCap)

    ####################################################################################################################
    # sampleEndCapInput
    SpiralizerMeshNode.samplesEndCap = nAttr.create(endCapsLong,
                                                    endCapsShort,
                                                    om.MFnNumericData.kInt, 2)
    nAttr.setMin(1)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.samplesEndCap)

    ####################################################################################################################
    # twisterCylinderInput
    SpiralizerMeshNode.twisterCylinderInput = nAttr.create(twistLong,
                                                           twisterShort,
                                                           om.MFnNumericData.kFloat, 0.0)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.twisterCylinderInput)

    ####################################################################################################################
    # radiusSpiralRampInput
    SpiralizerMeshNode.radiusSpiralRampInput = om.MRampAttribute().createCurveRamp(radiusSpiralRampLong,
                                                                                   radiusSpiralRampShort)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.radiusSpiralRampInput)

    ####################################################################################################################
    # radiusSpiralRampOptionsInput
    SpiralizerMeshNode.radiusSpiralRampOptionsInput = eAttr.create(radiusOptionsLong,
                                                                   radiusOptionsShort)
    eAttr.addField('Full path', 0)
    eAttr.addField('Spiral', 1)
    eAttr.setWritable(True)
    eAttr.setStorable(True)
    eAttr.setKeyable(True)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.radiusSpiralRampOptionsInput)

    ####################################################################################################################
    # distributionRampInput
    SpiralizerMeshNode.distributionRampInput = om.MRampAttribute().createCurveRamp(distributionSpiralRampLong,
                                                                                   distributionSpiralRampShort)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.distributionRampInput)

    ####################################################################################################################
    # distributionOptionsInput
    SpiralizerMeshNode.distributionOptionsInput = eAttr.create(distributionOptionsLong,
                                                               distributionOptionsLong)
    eAttr.addField('Full path', 0)
    eAttr.addField('Spiral', 1)
    eAttr.setWritable(True)
    eAttr.setStorable(True)
    eAttr.setKeyable(True)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.distributionOptionsInput)

    ####################################################################################################################
    # radiusCylinderRampInput
    SpiralizerMeshNode.radiusCylinderRampInput = om.MRampAttribute().createCurveRamp(radiusCylinderRampLong,
                                                                                     radiusCylinderRampShort)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.radiusCylinderRampInput)

    ####################################################################################################################
    # twistCylinderRampInput
    SpiralizerMeshNode.twistCylinderRampInput = om.MRampAttribute().createCurveRamp(twistCylinderRampLong,
                                                                                    twistCylinderRampShort)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.twistCylinderRampInput)

    ####################################################################################################################
    # offsetInput
    SpiralizerMeshNode.offsetInput = nAttr.create(offsetLong,
                                                  offsetShort,
                                                  om.MFnNumericData.kFloat, 0.0)
    nAttr.setWritable(True)
    nAttr.setStorable(True)
    nAttr.setKeyable(True)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.offsetInput)

    ####################################################################################################################
    # meshOutput
    SpiralizerMeshNode.meshOutput = typedAttr.create(meshOutputLong,
                                                     meshOutputShort,
                                                     om.MFnMeshData.kMesh)
    typedAttr.setReadable(True)
    typedAttr.setWritable(False)
    SpiralizerMeshNode.addAttribute(SpiralizerMeshNode.meshOutput)

    ####################################################################################################################
    # relationships
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.curveInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.clockwiseInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.percentInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.samplesUInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.samplesVInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.samplesStartCap, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.samplesEndCap, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.twisterCylinderInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.turnsInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.radiusSpiralInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.radiusCylinderInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.radiusSpiralRampInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.radiusSpiralRampOptionsInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.distributionRampInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.distributionOptionsInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.radiusCylinderRampInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.twistCylinderRampInput, SpiralizerMeshNode.meshOutput)
    SpiralizerMeshNode.attributeAffects(SpiralizerMeshNode.offsetInput, SpiralizerMeshNode.meshOutput)


########################################################################################################################
########################################################################################################################
# Create command to apply SpiralizerMesh in a curve/path
kCmdName = 'makeMeshSpiral'


class SpiralizerMeshCmd(ompx.MPxCommand):
    def __init__(self):
        ompx.MPxCommand.__init__(self)
        self.mArgList = om.MArgList()
        self.mdgMod = om.MDGModifier()
        self.mdagMod = om.MDagModifier()
        self.sel = om.MSelectionList()
        self.mfnDep = om.MFnDependencyNode()

    # override
    def isUndoable(self):
        return True

    # override
    def doIt(self, mArgList):
        # Clear all arrays
        self.sel.clear()

        # Get size of mArgList
        sizeMArgList = mArgList.length()
        # If have some arguments...
        if sizeMArgList > 0:
            for i in range(sizeMArgList):
                arg = mArgList.asString(i)
                try:
                    self.sel.add(arg)
                except RuntimeError:
                    print '"{}" not exist.'.format(arg)
        # ... otherwise get selected curves
        else:
            om.MGlobal.getActiveSelectionList(self.sel)

        # Call redoIt()
        self.redoIt()

    # override
    def undoIt(self):
        self.mdgMod.undoIt()
        self.mdagMod.undoIt()

    # override
    def redoIt(self):
        # Reset mdgMod and mdagMod
        self.mdgMod = om.MDGModifier()
        self.mdagMod = om.MDagModifier()

        # Iterate over all sel itens
        mItSelectionList = om.MItSelectionList(self.sel)
        # If confirmed the selection item is a curve, connect it with a SpiralizerMesh
        while not mItSelectionList.isDone():
            selectionObj = om.MObject()
            mItSelectionList.getDependNode(selectionObj)
            # Verify if selection is a kTransform
            if selectionObj.apiType() == om.MFn.kTransform:
                # Need get dagPath to get your child (shape)
                selectionDagPath = om.MDagPath()
                om.MDagPath.getAPathTo(selectionObj, selectionDagPath)
                # Get curve name
                originalCurveNode = selectionDagPath.node()
                self.mfnDep.setObject(originalCurveNode)
                originalCurveName = self.mfnDep.name()
                originalCurveName = originalCurveName.replace(':', '_')
                if selectionDagPath.childCount() > 0:
                    # Verify if child is a nurbsCurve
                    child = selectionDagPath.child(0)
                    if child.apiType() == om.MFn.kNurbsCurve:
                        self.mfnDep.setObject(child)
                        curveMPlugArray = self.mfnDep.findPlug('worldSpace', True)
                        curveWorldSpaceMPlug = curveMPlugArray.elementByLogicalIndex(0)

                        # Create a new SpiralizerMesh node and made a connection with this spline
                        spiralizerMeshNode = self.mdgMod.createNode(kPluginNodeTypeName)
                        self.mfnDep.setObject(spiralizerMeshNode)
                        spiralMPlug = self.mfnDep.findPlug(curveInputLong)
                        self.mdgMod.connect(curveWorldSpaceMPlug, spiralMPlug)
                        self.mdgMod.doIt()

                        # Create a new empty mesh(transform)
                        resultMeshTransform = self.mdagMod.createNode('mesh')
                        self.mdagMod.renameNode(resultMeshTransform, 'spiral_{}'.format(originalCurveName))
                        self.mdagMod.doIt()

                        # Get child from resultMeshTransform
                        resultMeshDagPath = om.MDagPath()
                        om.MDagPath.getAPathTo(resultMeshTransform, resultMeshDagPath)
                        resultMeshShape = resultMeshDagPath.child(0)

                        # Get MPlugs from spiralizerMesh node and resultMeshShape
                        self.mfnDep.setObject(spiralizerMeshNode)
                        spiralizerOutputMeshMPlug = self.mfnDep.findPlug(meshOutputLong)
                        self.mfnDep.setObject(resultMeshShape)
                        resultInputMeshMPlug = self.mfnDep.findPlug('inMesh')

                        self.mdgMod.connect(spiralizerOutputMeshMPlug, resultInputMeshMPlug)
                        self.mdgMod.doIt()

            mItSelectionList.next()


def spiralizerMeshCmdCreator():
    return ompx.asMPxPtr(SpiralizerMeshCmd())


# Create array to store the menu for spiralizerMesh
menuArray = []
# Name subMenu
subMenuName = 'jum'

def initializePlugin(mobj):
    mplugin = ompx.MFnPlugin(mobj)
    try:
        mplugin.registerNode(kPluginNodeTypeName,
                             spiralizerMeshID,
                             spiralizerMeshNodeCreator,
                             spiralizerMeshInitializer)
    except:
        sys.stderr.write('Failed to register node: {}'.format(kPluginNodeTypeName))
        raise

    try:
        mplugin.registerCommand(kCmdName,
                                spiralizerMeshCmdCreator)
    except:
        sys.stderr.write('Failed to register cmd: {}'.format(kCmdName))
        raise

    # Add spiralizer to the Create menu
    # Create, if necessary, a jMenu
    if not cmds.menuItem(subMenuName, ex=True):
        cmds.menuItem(subMenuName, subMenu=True, label=subMenuName, parent='mainCreateMenu')

    menuCreated = mplugin.addMenuItem('Spiralizer Mesh', 'mainCreateMenu|' + subMenuName, kCmdName, '')
    menuArray.append(menuCreated)

def uninitializePlugin(mobj):
    mplugin = ompx.MFnPlugin(mobj)
    try:
        mplugin.deregisterNode(spiralizerMeshID)
    except:
        sys.stderr.write('Failed to deregister node: {}'.format(kPluginNodeTypeName))
        raise

    try:
        mplugin.deregisterCommand(kCmdName)
    except:
        sys.stderr.write('Failed to deregister cmd: {}'.format(kCmdName))
        raise

    # Remove spiralizer from Create menu
    mplugin.removeMenuItem(menuArray[0])

    if cmds.menuItem(subMenuName, ex=True):
        childsMenu = cmds.menu(subMenuName, q=True, ia=True)
        if childsMenu == None:
            cmds.deleteUI(subMenuName, menuItem=True)
